# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)
  str.delete!(('a'..'z').to_a.join)
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)
  str.size.odd? ? str[(str.size / 2)] : str[((str.size / 2) - 1)..(str.size / 2)]
end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)
  str.downcase.count VOWELS.join
end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)
  (1..num).reduce(1, :*)
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  new_string = ''
  (0...arr.size).each do |i|
    new_string << (i < (arr.size - 1) ? (arr[i] + separator) : arr[i])
  end
  new_string
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  new_string = ''
  str.each_char.with_index do |char, i|
    new_string << (i.even? ? char.downcase : char.upcase)
  end
  new_string
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)
  new_string = []
  str.split.each { |x| new_string << (x.size > 4 ? x.reverse : x) }
  new_string.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)
  fb_array = []
  (1..n).each do |i|
    if (i % 3 == 0) && (i % 5 == 0)
      fb_array << 'fizzbuzz'
    elsif i % 3 == 0
      fb_array << 'fizz'
    elsif i % 5 == 0
      fb_array << 'buzz'
    else
      fb_array << i
    end
  end
  fb_array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  # arr.reverse
  new_arr = []
  arr.each { |i| new_arr.unshift(i) }
  new_arr
end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)
  (2...num).each { |i| return false if (num % i).zero? }
  num > 1
end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)
  num_factors = []
  (1..num).each { |i| num_factors << i if (num % i).zero? }
  num_factors
end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)
  prime_facts = []
  factors(num).each { |i| prime_facts << i if prime?(i) }
  prime_facts
end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)
  prime_factors(num).size
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)
  odds = []
  evens = []
  arr.each { |i| i.even? ? odds << i : evens << i }
  odds.size == 1 ? odds[0] : evens[0]
end
